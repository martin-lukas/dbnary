package org.getalp.dbnary;

import java.util.HashSet;

public abstract class InflectionData {

  public abstract HashSet<PropertyObjectPair> toPropertyObjectMap();

}
