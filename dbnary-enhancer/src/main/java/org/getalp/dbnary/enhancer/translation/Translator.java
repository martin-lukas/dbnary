package org.getalp.dbnary.enhancer.translation;

public interface Translator {

  public String translate(String source, String slang, String tlang);

}
